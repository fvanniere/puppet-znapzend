# Changelog

All notable changes to this project will be documented in this file.

## Release 1.0.1

* Changed main parameter structure to allow full defaulting of backup properties.

## Release 1.0.2

* Documentation update.
* Puppet lint fixes.

**Features**

**Bugfixes**

**Known Issues**
