
# znapzend

This modules installs and configures
[ZnapZend](http://www.znapzend.org). ZnapZend provides high performance open
source ZFS backup with mbuffer and ssh support.

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with znapzend](#setup)
    * [What znapzend affects](#what-znapzend-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with znapzend](#beginning-with-znapzend)
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and
   how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)

## Description

The ZnapZend module does the following:

 - Installs ZnapZend from a package that you supply. An .rpm SPEC file is
   available, see <https://github.com/asciiphil/znapzend-spec>
 
 - Configures ZnapZend via zfs dataset properties.
 
 - Manages the 'znapzend' service.
 

## Setup

### What znapzend affects

This module has the following dependencies:

* [caius/zfs_property](https://forge.puppet.com/caius/zfs_property) Puppet
  module, which provides the functionality to get/set ZFS dataset properties.
* [puppetlabs/stdlib](https://forge.puppet.com/puppetlabs/stdlib)

### Setup Requirements

* ZFS on Linux must be installed, see [Native ZFS on
  Linux](https://zfsonlinux.org/)

### Beginning with znapzend

The module supplies sensible defaults for other values, via included Hiera
configuration.

This is the minimum configuration required (for local snapshots via a defaulted snapshot plan):

```puppet
class { 'znapzend':
  dataset_backups => 
    [
      { 'dataset' => 'tank/dataset1' }
	]
}
```

Using Puppet and Hiera:

```puppet
include znapzend
```

```yaml
znapzend::dataset_backups:
  - dataset: 'tank/dataset1'
```
     
A default source dataset plan of hourly backups kept for a day, and daily
backups kept for a week will be applied.

## Usage

A more complete example using most of the module features, using Puppet and
Hiera:

```puppet
include znapzend
```

```yaml
  znapzend::dataset_backups:
    - dataset: nmsstore1
      config:
        destinations:
          remote:
            dst: root@foo.example.com
            dst_plan:
              - keep: 10 years
                freq: 1 year
          remote2:
            dst: root@some.other.host
            dst_plan:
              - keep: 1 month
                freq: 1 day
              - keep: 6 months
                freq: 1 week
    - dataset: tank2/home
      config:
        overrides:
          enabled: false
          mbuffer_size: 2G
        destinations:
          totank1:
            dst: root@tank1.example.com
            dst_plan:
              - keep: 1 week
                freq: 30 minutes
```

## Reference

Please see the Puppet Strings generated documentation for a list of classes,
parameters, etc.

## Limitations

Tested using Puppet 5.5 and CentOS 7.

## Development

Pull requests/issues welcome.
